# Partie 1 : Préparation de la machine `backup.tp6.linux`

# I. Ajout de disque

🌞 **Ajouter un disque dur de 5Go à la VM `backup.tp6.linux`**

```bash
[eric@backup ~]$ lsblk
[...]
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    5G  0 disk
[...]
```

# II. Partitioning

🌞 **Partitionner le disque à l'aide de LVM**

```bash

# ajout PV

[eric@backup ~]$ sudo pvcreate /dev/sdb
[sudo] password for eric:
  Physical volume "/dev/sdb" successfully created.

# verification

[eric@backup ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g    0
  /dev/sdb      lvm2 ---   5.00g 5.00g

# ajout VG

[eric@backup ~]$ sudo vgcreate backup /dev/sdb
  Volume group "backup" successfully created

# verification

[eric@backup ~]$ sudo vgs
  VG     #PV #LV #SN Attr   VSize  VFree
  backup   1   0   0 wz--n- <5.00g <5.00g
  rl       1   2   0 wz--n- <7.00g     0

# ajout LV

[eric@backup ~]$ sudo lvcreate -L 4.99G backup -n backup
  Rounding up size to full physical extent 4.99 GiB
  Logical volume "backup" created.

# verification 

[eric@backup ~]$ sudo lvs
  LV     VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  backup backup -wi-a-----   4.99g
  root   rl     -wi-ao----  <6.20g
  swap   rl     -wi-ao---- 820.00m


```

🌞 **Formater la partition**

```bash
[eric@backup ~]$ sudo mkfs -t ext4 /dev/backup/backup
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 1308672 4k blocks and 327680 inodes
Filesystem UUID: b6cda463-7781-4125-a194-42a1474eba05
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
```

🌞 **Monter la partition**

```bash

# monter une partition

[eric@backup ~]$ sudo mkdir /mnt/backup1
[eric@backup ~]$ sudo mount /dev/backup/backup /mnt/backup1

# verification

[eric@backup ~]$ df -h
[...]
/dev/mapper/backup-backup  4.9G   20M  4.6G   1% /mnt/backup1
```

# III. Bonus

➜ Ajouter un deuxième disque de 5Go à la VM et faire une partition de 10Go

- faites en un PV
- ajoutez le au VG existant (il fait donc 10 Go maintenant)
- étendez la partition à 10Go
- prouvez que la partition utilisable fait 10Go désormais

[Partie 2 : Setup du serveur NFS sur `backup.tp6.linux`](./part2.md).