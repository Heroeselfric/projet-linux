# TP6 : Stockage et sauvegarde

# Sommaire

Les différentes parties du TP :

- [Partie 1 : Préparation de la machine `backup.tp6.linux`](./part1.md)
- [Partie 2 : Setup du serveur NFS sur `backup.tp6.linux`](./part2.md)
- [Partie 3 : Setup des clients NFS : `web.tp6.linux` et `db.tp6.linux`](./part3.md)
- [Partie 4 : Scripts de sauvegarde](./part4.md)

