# Partie 3 : Setup des clients NFS : `web.tp6.linux` et `db.tp6.linux`

🌞 **Install'**

```bash
[eric@web ~]$ sudo dnf install nfs-utils
[sudo] password for eric:
[...]
Installed:
  gssproxy-0.8.0-19.el8.x86_64                        keyutils-1.5.10-9.el8.x86_64                        libverto-libevent-0.3.0-5.el8.x86_64                        nfs-utils-1:2.3.3-46.el8.x86_64                        rpcbind-1.2.5-8.el8.x86_64

Complete!
```

🌞 **Conf'**

```bash
[eric@web ~]$ sudo mkdir /srv/backup
```

```bash
[eric@web etc]$ sudo nano idmapd.conf
[...]
Domain = tp6.linux
[...]
```



🌞 **Montage !**


```bash
[eric@web backup]$ sudo mount -t nfs 10.5.1.13:/backup/web.tp6.linux/ /srv/backup/
```

```bash
[eric@web backup]$ df -h
[...]
10.5.1.13:/backup/web.tp6.linux  9.8G   36M  9.3G   1% /srv/backup
```

```bash

```

🌞 **Répétez les opérations sur `db.tp6.linux`**

```bash
[eric@db backup]$ sudo mount -t nfs 10.5.1.13:/backup/db.tp6.linux/ /srv/backup/
```

```bash
[eric@db ~]$  df -h
[...]
10.5.1.13:/backup/db.tp6.linux  9.8G   36M  9.3G   1% /srv/backup
```

```bash

```

Final step : [mettre en place la sauvegarde, c'est la partie 4](./part4.md).
