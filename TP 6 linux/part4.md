# Partie 4 : Scripts de sauvegarde

## I. Sauvegarde Web

🌞 **Ecrire un script qui sauvegarde les données de NextCloud**

```

```

🌞 **Créer un service**

```

```

🌞 **Vérifier que vous êtes capables de restaurer les données**

```

```

🌞 **Créer un *timer***

```

```
## II. Sauvegarde base de données

🌞 **Ecrire un script qui sauvegarde les données de la base de données MariaDB**

```

```

🌞 **Créer un service**

```

```

🌞 **Créer un `timer`**

```

```
