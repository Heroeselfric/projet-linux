# Partie 2 : Setup du serveur NFS sur `backup.tp6.linux`


🌞 **Préparer les dossiers à partager**

```bash

# ajout des dossiers

[eric@backup backup]$ sudo mkdir web.tp6.linux
[eric@backup backup]$ sudo mkdir db.tp6.linux

# verification 

[eric@backup backup]$ ls
backup  db.tp6.linux  web.tp6.linux
```

🌞 **Install du serveur NFS**

```
[eric@backup backup]$ sudo dnf install nfs-utils
[...]
Installed:
  gssproxy-0.8.0-19.el8.x86_64                        keyutils-1.5.10-9.el8.x86_64                        libverto-libevent-0.3.0-5.el8.x86_64                        nfs-utils-1:2.3.3-46.el8.x86_64                        rpcbind-1.2.5-8.el8.x86_64

Complete!
```

🌞 **Conf du serveur NFS**

```bash

# dans /etc/idmapd.conf
[...]
Domain = tp6.linux
[...]

# dans /etc/exports

/backup/web.tp6.linux/ 10.5.1.11/24(rw,no_root_squash)
/backup/db.tp6.linux/ 10.5.1.12/24(rw,no_root_squash)

# rw : permet de mettre les fichier en lecture et ecriture.
# no_root_squash : interdit aux user de posseder les droits root
```

🌞 **Démarrez le service**

```bash
[eric@backup /]$ sudo systemctl enable --now nfs-server.service
[sudo] password for eric:
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[eric@backup /]$ sudo systemctl status nfs-server.service
● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; enabled; vendor preset: disabled)
  Drop-In: /run/systemd/generator/nfs-server.service.d
           └─order-with-mounts.conf
   Active: active (exited) since Tue 2021-12-07 14:13:51 CET; 33s ago
  Process: 26405 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then systemctl reload gssproxy ; fi (code=exited, status=0/SUCCESS)
  Process: 26393 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
  Process: 26391 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=1/FAILURE)
 Main PID: 26405 (code=exited, status=0/SUCCESS)
```

🌞 **Firewall**

```bash
[eric@backup /]$ sudo firewall-cmd --zone=public --add-port=2049/tcp
success
[eric@backup /]$ sudo firewall-cmd --zone=public --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 2049/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

# verification 

[eric@backup /]$ ss -lntp | grep 2049
LISTEN 0      64           0.0.0.0:2049       0.0.0.0:*
LISTEN 0      64              [::]:2049          [::]:*
```

[la partie 3 pour setup les clients NFS](./part3.md).
