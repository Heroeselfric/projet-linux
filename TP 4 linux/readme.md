# TP4 : Une distribution orientée serveur 

--- 

*** 🌞 Choisissez et définissez une IP à la VM ***

ip a

```
[eric@localhost ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:8b:49:be brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 84594sec preferred_lft 84594sec
    inet6 fe80::a00:27ff:fe8b:49be/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:7f:6f:ef brd ff:ff:ff:ff:ff:ff
    inet 10.250.1.2/24 brd 10.250.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe7f:6fef/64 scope link
       valid_lft forever preferred_lft forever
```

cat ifcfg enp0s8 

```
[eric@localhost network-scripts]$ cat ifcfg-enp0s8
TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.250.1.2
NETMASK=255.255.255.0
```

*** 🌞 Connexion ssh fonctionelle ***

```
[eric@localhost network-scripts]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Tue 2021-11-23 16:46:29 CET; 13min ago
     Docs: man:sshd(8)
```
*** 🌞 Prouvez que vous avez un accès internet ***

```
[eric@localhost network-scripts]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=27.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=28.1 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=26.4 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=25.8 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3009ms
rtt min/avg/max/mdev = 25.813/26.954/28.131/0.908 ms
```

*** 🌞 Prouvez que vous avez de la résolution de nom ***

```
[eric@localhost network-scripts]$ ping youtube.com
PING youtube.com (142.250.179.78) 56(84) bytes of data.
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=1 ttl=113 time=25.7 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=2 ttl=113 time=25.1 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=3 ttl=113 time=26.9 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=4 ttl=113 time=25.5 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=5 ttl=113 time=27.3 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=6 ttl=113 time=25.2 ms
^C
--- youtube.com ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 5020ms
rtt min/avg/max/mdev = 25.138/25.947/27.294/0.859 ms
```

*** 🌞 Définissez node1.tp4.linux comme nom à la machine ***

```
[eric@localhost network-scripts]$ cat /etc/hostname
node1.tp4.linux
```

```
[eric@localhost network-scripts]$ hostname
node1.tp4.linux
```

*** 🌞 Installez NGINX en vous référant à des docs online ***

```
[eric@node1 ~]$ sudo dnf upgrade
Last metadata expiration check: 16:41:22 ago on Tue 23 Nov 2021 04:44:46 PM CET.
Dependencies resolved.
Nothing to do.
Complete!
[eric@node1 ~]$ sudo dnf install nginx
[sudo] password for eric:
Last metadata expiration check: 16:47:46 ago on Tue 23 Nov 2021 04:44:46 PM CET.
Package nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
[eric@node1 ~]$ sudo systemctl start nginx
[sudo] password for eric:
[eric@node1 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-11-24 09:58:12 CET; 15s ago
  Process: 5383 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 5380 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 5378 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 5384 (nginx)
```

*** 🌞 Analysez le service NGINX ***

```
[eric@node1 ~]$ ps -ef
[...]
root        1486       1  0 15:54 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1487    1486  0 15:54 ?        00:00:00 nginx: worker process
nginx       1488    1486  0 15:54 ?        00:00:00 nginx: worker process
nginx       1489    1486  0 15:54 ?        00:00:00 nginx: worker process
```

```
[eric@node1 ~]$ ss -laputen | grep tcp
tcp   LISTEN 0      128             0.0.0.0:22        0.0.0.0:*     ino:22090 sk:1051 <->
tcp   LISTEN 0      128             0.0.0.0:80        0.0.0.0:*     ino:26174 sk:1052 <->
tcp   ESTAB  0      36           10.250.1.2:22     10.250.1.1:61510 timer:(on,238ms,0) ino:25195 sk:58 <->
tcp   LISTEN 0      128                [::]:22           [::]:*     ino:22092 sk:1053 v6only:1 <->
tcp   LISTEN 0      128                [::]:80           [::]:*     ino:26175 sk:1054 v6only:1 <->
```

```
[eric@node1 ~]$ cat /etc/nginx/nginx.conf | grep nginx | grep root
[...]
        root         /usr/share/nginx/html;
[...]
```

```
[eric@node1 ~]$ ls -al /usr/share/nginx/html/
total 20
drwxr-xr-x. 2 root root   99 Nov 23 17:14 .
drwxr-xr-x. 4 root root   33 Nov 23 17:14 ..
-rw-r--r--. 1 root root 3332 Jun 10 11:09 404.html
-rw-r--r--. 1 root root 3404 Jun 10 11:09 50x.html
-rw-r--r--. 1 root root 3429 Jun 10 11:09 index.html
-rw-r--r--. 1 root root  368 Jun 10 11:09 nginx-logo.png
-rw-r--r--. 1 root root 1800 Jun 10 11:09 poweredby.png
```


*** Firewall pour autorisation du trafic du service NGINX ***

```
[eric@node1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
[sudo] password for eric:
success
[eric@node1 ~]$ sudo firewall-cmd --reload
success
```

*** 🌞 Tester le bon fonctionnement du service *** 

```
[eric@node1 ~]$ sudo curl 
```

*** 🌞 Changer le port d'écoute ***

```
[eric@node1 ~]$ sudo nano /etc/nginx/nginx.conf
[...]
    server {
        listen       8080 default_server;
        listen       [::]:8080 default_server;
        server_name  _;
        root         /usr/share/nginx/html;
[...]
```

```
[eric@node1 ~]$ sudo systemctl restart nginx
```

```
[eric@node1 ~]$ ss -laputen | grep 8080
tcp   LISTEN 0      128             0.0.0.0:8080      0.0.0.0:*     ino:28370 sk:1005 <->
tcp   LISTEN 0      128                [::]:8080         [::]:*     ino:28371 sk:1006 v6only:1 <->
```

```
[eric@node1 ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[eric@node1 ~]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success
[eric@node1 ~]$ sudo firewall-cmd --reload
success
```

visite vers le port 8080

```
[eric@node1 ~]$ curl -UseBasicParsing http://10.250.1.2:8080/
Enter proxy password for user 'seBasicParsing':
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
```

*** 🌞 Changer l'utilisateur qui lance le service *** 

```

```

*** 🌞 Changer l'emplacement de la racine Web *** 

```

```