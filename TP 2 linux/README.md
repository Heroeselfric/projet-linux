# TP2 : Explorer et manipuler le systèm

--- 

**🌞 Changer le nom de la machine**

```
eric@node1:~$ sudo hostname node1.tp2.linux
eric@node1:~$ sudo nano /etc/hostname
    node1.tp2.linux

eric@node1:~$ cat /etc/hostname
node1.tp2.linux

```

**🌞 Config réseau fonctionelle**

Sur la VM
```
    eric@node1:~$ ping 1.1.1.1
    PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
    64 bytes from 1.1.1.1: icmp_seq=1 ttl=63 time=2.39 ms
    64 bytes from 1.1.1.1: icmp_seq=2 ttl=63 time=2.34 ms
    64 bytes from 1.1.1.1: icmp_seq=3 ttl=63 time=2.52 ms

    eric@node1:~$ ping ynov.com
    PING ynov.com (92.243.16.143) 56(84) bytes of data.
    64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=50 time=21.0 ms
    64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=50 time=22.1 ms
    64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=3 ttl=50 time=21.8 ms
```

Sur Mon ordinateur

```
    C:\Users\heroeselfric>ping 192.168.177.8
    Envoi d’une requête 'Ping'  192.168.177.8 avec 32 octets de données :
    Réponse de 192.168.177.8 : octets=32 temps<1ms TTL=64
    Réponse de 192.168.177.8 : octets=32 temps<1ms TTL=64
    Réponse de 192.168.177.8 : octets=32 temps<1ms TTL=64
```

## 1ere partie, SSH

**🌞 Installation du serveur**

```
    eric@node1:~$ sudo apt install openssh-server
```

**🌞 Lancement du Service SSH**

```
    eric@node1:~$ systemctl start ssh 
    eric@node1:~$ systemctl status ssh 
        [...]
        Active : active (runninig)
        [...]
```
**🌞 Analyser le service en cours de fonctionnement**

```
    eric@node1:~$ systemctl status ssh 
        [...]
        Active : active (runninig)
        [...]
    eric@node1:~$ ps ssh
        [...]
        1000    1918 0000000000000000 0000000000000000 0000000000000000 0000000073d1fef9 R+   pts/1      0:00 ps ssh
        [...]
    eric@node1:~$ ss -l
        [...]
        tcp   LISTEN 0      128                                       0.0.0.0:ssh                       0.0.0.0:*
        tcp   LISTEN 0      128                                          [::]:ssh                          [::]:*
        [...]
    eric@node1:~$ journalctl
        [...]
        nov. 07 08:30:45 node1.tp2.linux sshd[537]: Server listening on 0.0.0.0 port 22.
        nov. 07 08:30:45 node1.tp2.linux sshd[537]: Server listening on :: port 22.
        [...]
    eric@node1:~$ cd /var/log
        cd journal
        [...]
        user: name=sshd, UID=123, GID=65534, home=/run/sshd, shell=/usr/sbin/nologin, from=none
        [...]
```
**🌞 Modifier le comportement du serveur**

```
    eric@node1:~$ cd /etc/ssh
        eric@node1:~$ sudo nano sshd_config
        [...]
    eric@node1:~$ cat sshd_config
        [...]
        Port 9999
        [...]
    eric@node1:~$ ss -l 
        [...]
        tcp   LISTEN 0      128                                       0.0.0.0:9999                      0.0.0.0:*
        [...]
        tcp   LISTEN 0      128                                          [::]:9999                         [::]:*
        [...]
```

## 2ème partie, FTP

**🌞 installation du serveur**
```
    eric@node1:~$ sudo apt install vsftpd
```
**🌞 lancement du service FTP**
```
        eric@node1:~$ systemctl start vsftpd
        ==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ===
        Authentication is required to start 'vsftpd.service'.
        Authenticating as: Eric,,, (eric)
        Password:
        ==== AUTHENTICATION COMPLETE ===

        eric@node1:~$ systemctl status vsftpd
    ● vsftpd.service - vsftpd FTP server
        Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
        Active: active (running) since Mon 2021-11-08 19:08:24 CET; 3min 12s ago
        Process: 1749 ExecStartPre=/bin/mkdir -p /var/run/vsftpd/empty (code=exited, status=0/SUCCESS)
        Main PID: 1751 (vsftpd)
        Tasks: 1 (limit: 2314)
        Memory: 676.0K
            CPU: 4ms
        CGroup: /system.slice/vsftpd.service
                └─1751 /usr/sbin/vsftpd /etc/vsftpd.conf

    nov. 08 19:08:24 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
    nov. 08 19:08:24 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
```
**🌞 Etude du service FTP**
```
        eric@node1:~$ systemctl status vsftpd
    ● vsftpd.service - vsftpd FTP server
        Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
        Active: active (running) since Mon 2021-11-08 19:08:24 CET; 3min 12s ago
        Process: 1749 ExecStartPre=/bin/mkdir -p /var/run/vsftpd/empty (code=exited, status=0/SUCCESS)
        Main PID: 1751 (vsftpd)
        Tasks: 1 (limit: 2314)
        Memory: 676.0K
            CPU: 4ms
        CGroup: /system.slice/vsftpd.service
                └─1751 /usr/sbin/vsftpd /etc/vsftpd.conf

    nov. 08 19:08:24 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
    nov. 08 19:08:24 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
        eric@node1:~$ ps -e | grep vsftpd
            1751 ?        00:00:00 vsftpd
        eric@node1:~$ ss -l
            [...]
            tcp   LISTEN 0      32                                              *:ftp                             *:*
            [...]
        eric@node1:~$ journalctl | grep vsftpd
            nov. 08 19:08:21 node1.tp2.linux sudo[1589]:     eric : TTY=pts/0 ; PWD=/home/eric ; USER=root ; COMMAND=/usr/bin/apt install vsftpd
            nov. 08 19:08:24 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
            nov. 08 19:08:24 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
            nov. 08 19:10:52 node1.tp2.linux polkitd(authority=local)[449]: Operator of unix-process:2238:121226 successfully authenticated as unix-user:eric to gain ONE-SHOT authorization for action org.freedesktop.systemd1.manage-units for system-bus-name::1.91 [systemctl start vsftpd] (owned by unix-user:eric)
```
**🌞 Modification de la configuration du serveur**