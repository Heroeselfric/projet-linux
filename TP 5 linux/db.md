# I. Setup DB



## Sommaire

- [I. Setup DB](#i-setup-db)
  - [Sommaire](#sommaire)
  - [1. Install MariaDB](#1-install-mariadb)
  - [2. Conf MariaDB](#2-conf-mariadb)
  - [3. Test](#3-Test)

## 1. Install MariaDB


🌞 **Installer MariaDB sur la machine db.tp5.linux**

```
[eric@db ~]$ sudo dnf install mariadb-server
[...]
Installed:
  mariadb-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64                mariadb-backup-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-common-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64         mariadb-connector-c-3.1.11-2.el8_3.x86_64
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch                      mariadb-errmsg-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-gssapi-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64  mariadb-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-server-utils-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64   perl-DBD-MySQL-4.046-3.module+el8.4.0+577+b8fe2d92.x86_64
  perl-DBI-1.641-3.module+el8.4.0+509+59a8d9b3.x86_64                   perl-Math-BigInt-1:1.9998.11-7.el8.noarch
  perl-Math-Complex-1.59-420.el8.noarch                                 psmisc-23.1-5.el8.x86_64

Complete!
```

🌞 **Le service MariaDB**

```
[eric@db ~]$ systemctl start mariadb
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'mariadb.service'.
Authenticating as: eric
Password:
==== AUTHENTICATION COMPLETE ====
```

```
[eric@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```

```
[eric@db ~]$ systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-11-25 17:14:08 CET; 1min 9s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
 Main PID: 6720 (mysqld)
```

```
[eric@db ~]$ ss -alnpt
State          Recv-Q         Send-Q                   Local Address:Port                   Peer Address:Port         Process
LISTEN         0              128                            0.0.0.0:22                          0.0.0.0:*
LISTEN         0              80                                   *:3306                              *:*
LISTEN         0              128                               [::]:22                             [::]:*                      *:*
```

```
[eric@db ~]$ ps -ef | grep mysql
mysql       6720       1  0 17:14 ?        00:00:01 /usr/libexec/mysqld --basedir=/usr
eric        6857    5412  0 17:22 pts/0    00:00:00 grep --color=auto mysql
```

🌞**Firewall** 

```
[eric@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
[sudo] password for eric:
success
[eric@db ~]$ sudo firewall-cmd --reload
success
```
## 2. Conf MariaDB

🌞 **Configuration élémentaire de la base**

```
[eric@db ~]$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] Y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] Y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] Y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] Y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] Y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

🌞 **Préparation de la base en vue de l'utilisation par NextCloud**

```
[eric@db ~]$ sudo mysql -u root -p
[sudo] password for eric:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 20
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.5.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.002 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.5.1.11';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```

## 3. Test

🌞 **Installez sur la machine web la commande mysql**

```
[eric@web ~]$ dnf provides mysql
[...]
Installed:
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch                      mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
  mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

Complete!
```

```
[eric@web ~]$ mysql -h 10.5.1.12 -P 3306 -u nextcloud -D nextcloud -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 25
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> SHOW TABLES;
Empty set (0.00 sec)
```

