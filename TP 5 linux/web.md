# II. Setup Web

## Sommaire

- [II. Setup Web](#ii-setup-web)
  - [Sommaire](#sommaire)
  - [1. Install Apache](#1-install-apache)
    - [A. Apache](#a-apache)
    - [B. PHP](#b-php)
  - [2. Conf Apache](#2-conf-apache)
  - [3. Install NextCloud](#3-install-nextcloud)
  - [4. Test](#4-test)

## 1. Install Apache

### A. Apache

🌞 **Installer Apache sur la machine web.tp5.linux**

```
[eric@web ~]$ sudo dnf install httpd
[sudo] password for eric:
[...]
Installed:
  apr-1.6.3-12.el8.x86_64                                         apr-util-1.6.1-6.el8.1.x86_64
  apr-util-bdb-1.6.1-6.el8.1.x86_64                               apr-util-openssl-1.6.1-6.el8.1.x86_64
  httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64              httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch
  httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64        mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64
  rocky-logos-httpd-85.0-3.el8.noarch

Complete!
```
🌞 **Analyse du service Apache**

```
[eric@web ~]$ systemctl start httpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'httpd.service'.
Authenticating as: eric
Password:
==== AUTHENTICATION COMPLETE ====
```

```
[eric@web ~]$ ps -ef
[...]
root        2426       1  0 15:18 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2427    2426  0 15:18 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2428    2426  0 15:18 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2429    2426  0 15:18 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2430    2426  0 15:18 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```

```
[eric@web ~]$ ss -anlpt
State          Recv-Q         Send-Q                   Local Address:Port                   Peer Address:Port         Process
LISTEN         0              128                            0.0.0.0:22                          0.0.0.0:*
LISTEN         0              128                                  *:80                                *:*
LISTEN         0              128                               [::]:22                             [::]:*
```
🌞 **Un premier test**

```
[eric@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
[sudo] password for eric:
success
```

### B. PHP


🌞 **Installer PHP**

```
[eric@web ~]$ sudo dnf install epel-release
[sudo] password for eric:
[...]
Installed:
  epel-release-8-13.el8.noarch

Complete!
```

```
[eric@web ~]$ sudo dnf update
Extra Packages for Enterprise Linux 8 - x86_64                                                        1.9 MB/s |  11 MB     00:05
Extra Packages for Enterprise Linux Modular 8 - x86_64                                                690 kB/s | 958 kB     00:01
Last metadata expiration check: 0:00:01 ago on Sun 28 Nov 2021 05:41:43 PM CET.
Dependencies resolved.
Nothing to do.
Complete!
```

```
[eric@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
[...]
Installed:
  remi-release-8.5-1.el8.remi.noarch

Complete!
```

```
[eric@web ~]$ sudo dnf module enable php:remi-7.4
[...]
Is this ok [y/N]: y
Complete!
```

```
[eric@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
[...]
Installed:
  environment-modules-4.5.2-1.el8.x86_64      libicu69-69.1-1.el8.remi.x86_64            libsodium-1.0.18-2.el8.x86_64
  oniguruma5php-6.9.7.1-1.el8.remi.x86_64     php74-libzip-1.8.0-1.el8.remi.x86_64       php74-php-7.4.26-1.el8.remi.x86_64
  php74-php-bcmath-7.4.26-1.el8.remi.x86_64   php74-php-cli-7.4.26-1.el8.remi.x86_64     php74-php-common-7.4.26-1.el8.remi.x86_64
  php74-php-fpm-7.4.26-1.el8.remi.x86_64      php74-php-gd-7.4.26-1.el8.remi.x86_64      php74-php-gmp-7.4.26-1.el8.remi.x86_64
  php74-php-intl-7.4.26-1.el8.remi.x86_64     php74-php-json-7.4.26-1.el8.remi.x86_64    php74-php-mbstring-7.4.26-1.el8.remi.x86_64
  php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64  php74-php-opcache-7.4.26-1.el8.remi.x86_64 php74-php-pdo-7.4.26-1.el8.remi.x86_64
  php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64 php74-php-process-7.4.26-1.el8.remi.x86_64 php74-php-sodium-7.4.26-1.el8.remi.x86_64
  php74-php-xml-7.4.26-1.el8.remi.x86_64      php74-runtime-1.0-3.el8.remi.x86_64        scl-utils-1:2.0.2-14.el8.x86_64
  tcl-1:8.6.8-2.el8.x86_64

Complete!
```

## 2. Conf Apache



🌞 **Analyser la conf Apache**

```
[eric@web ~]$ cat /etc/httpd/conf/httpd.conf | grep \.d
[...]
IncludeOptional conf.d/*.conf
```

🌞 **Créer un VirtualHost qui accueillera NextCloud**



```
[eric@web ~]$ cd /etc/httpd/conf.d
[eric@web conf.d]$ sudo nano vhost.conf

<VirtualHost *:80>

  DocumentRoot /var/www/nextcloud/html/

  ServerName  web.tp5.linux

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>

[eric@web conf.d]$ sudo systemctl restart httpd
```


🌞 **Configurer la racine web**

```
[eric@web conf.d]$ sudo mkdir -p /var/www/nextcloud/html
[sudo] password for eric:
```

```
[eric@web conf.d]$ sudo chown apache:apache /var/www/nextcloud/html
```


🌞 **Configurer PHP**

```
[eric@web conf.d]$ timedatectl
               Local time: Sun 2021-11-28 18:29:53 CET
           Universal time: Sun 2021-11-28 17:29:53 UTC
                 RTC time: Sun 2021-11-28 17:27:32
                Time zone: Europe/Paris   
```

```
[Date]
; Defines the default timezone used by the date functions
; http://php.net/date.timezone
;date.timezone = "Europe/Paris"
```


## 3. Install NextCloud



🌞 **Récupérer Nextcloud**

```
[eric@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0   486k      0  0:05:11  0:05:11 --:--:--  892k
[eric@web ~]$ ls
nextcloud-21.0.1.zip
```

🌞 **Ranger la chambre**

```
[eric@web ~]$ unzip nextcloud-21.0.1.zip
[eric@web ~]$ sudo mv nextcloud/* /var/www/nextcloud/html/
[sudo] password for eric:
[eric@web ~]$ sudo mv nextcloud/.htaccess /var/www/nextcloud/html/
[eric@web ~]$ sudo mv nextcloud/.user.ini /var/www/nextcloud/html/
[eric@web ~]$ sudo chown -R apache:apache /var/www/nextcloud/html
[eric@web ~]$ rm -R nextcloud
[eric@web ~]$ rm nextcloud-21.0.1.zip
```

## 4. Test

 

🌞 **Modifiez le fichier hosts de votre PC**



🌞 **Tester l'accès à NextCloud et finaliser son install'**



🔥🔥🔥 **Baboom ! Un beau NextCloud.**


