name=$(sudo cat /etc/hostname)
os=$(sudo cat /etc/os-release | grep -w "NAME" | cut -d '"' -f2)
kernel=$(uname -r)
ip=$(ip a | grep enp0s8 | cut -c 10- | grep -v BROAD | cut -d' ' -f1)
ram=$(free | grep Mem | tr -s '[:space:]' | cut -d' ' -f2)
ramm=$( free | grep Mem | tr -s '[:space:]' | cut -d' ' -f7)
disqueleft=$(df /dev/sda3 -h | tail -n 1 | tr -s " " | cut -d " " -f4 | rev | cut -c 2- | rev)
randomcat=$(curl -s --request GET 'https://api.thecatapi.com/v1/images/search?format=json' | grep -Eo "(http|https)://[a-zA-Z0-9./?=_%:-]*" | sort -u)

echo "Machine name: $name"
echo "OS : $os and kernel version is $kernel"
echo "IP : $ip"
echo "RAM : $ram $ramm"
echo "Disque : $disqueleft GO left"
echo "Top 5 processes by RAM usage : "
echo "- $(ps -eo %mem,pid,cmd | sort | tail -n 5 | head -n 1)"
echo "- $(ps -eo %mem,pid,cmd | sort | tail -n 2 | head -n 1)"
echo "- $(ps -eo %mem,pid,cmd | sort | tail -n 3 | head -n 1)"
echo "- $(ps -eo %mem,pid,cmd | sort | tail -n 4 | head -n 1)"
echo "- $(ps -eo %mem,pid,cmd | sort | tail -n 6 | head -n 1)"
echo "Listening ports :"
er=$(sudo ss -mlpt | grep users | awk '{print $6}' | head -n 1 | cut -d'"' -f2)
echo "- $(sudo ss -mlpt | grep users | awk '{print $3}' | head -n 1) : $er"
am=$(sudo ss -mlpt | grep users | awk '{print $6}' | head -n 2 | tail -n 1 | cut -d'"' -f2)
echo "- $(sudo ss -mlpt | grep users | awk '{print $3}' | head -n 2 | tail -n 1) : $am"
ma=$(sudo ss -mlpt | grep users | awk '{print $6}' | head -n 3 | tail -n 1 | cut -d'"' -f2)
echo "- $(sudo ss -mlpt | grep users | awk '{print $3}' | head -n 3 | tail -n 1) : $ma"
va=$(sudo ss -mlpt | grep users | awk '{print $6}' | head -n 4 | tail -n 1 | cut -d'"' -f2)
echo "- $(sudo ss -mlpt | grep users | awk '{print $3}' | head -n 4 | tail -n 1) : $va"
pa=$(sudo ss -mlpt | grep users | awk '{print $6}' | head -n 5 | tail -n 1 | cut -d'"' -f2)
echo "- $(sudo ss -mlpt | grep users | awk '{print $3}' | head -n 5 | tail -n 1) : $pa"
is=$(sudo ss -mlpt | grep users | awk '{print $6}' | head -n 6 | tail -n 1 | cut -d'"' -f2)
echo "- $(sudo ss -mlpt | grep users | awk '{print $3}' | head -n 6 | tail -n 1) : $is"
echo " "

echo "Here's your random cat : $randomcat"