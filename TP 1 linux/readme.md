# Tp are you dead yet ?

---

**🌞 Les différentes façons de détruire ça VM :**

### Première technique :
    Suprimer les fichier et les dossiers depuis la racines :
        sudo rm -R -no-preserve-root /

---

### Deuxième technique : 
    Lancer une boucle infinie en python dans le terminal 
        1. ouvrir le terminal
        2. creer un fichier main.py :
            § nano main.py
        3. creer la boucle infinie 
            § while True :
                print ("Lorem ipsum dolor sit amet. A quisquam dolore qui numquam distinctio eos accusantium nostrum aut doloribus rerum.")
        4. nano ~/.bashrc 
            § ecrire python3 main.py dans le bash 
            § changer les alias des commandes
                .alias cd=’python3 main.py’
		        .alias rm=’python3 main.py’
		        .alias cat=’python3 main.py’
		        .alias reboot=’python3 main.py’
		        .alias shutdown=’python3 main.py’
		        .alias pwd=’python3 main.py
		        .alias ps=’python3 main.py’
		        .alias stop=’python3 main.py’
		        .alias remove=’python3 main.py’
		        .alias ls=’python3 main.py’
		        .alias unalias=’python3 main.py’
		        .alias nano=’python3 main.py’
        5. fermer le terminal et l'ouvrir a nouveau 
        6. profiter de ce magnifique text latin !

### Troisieme technique :
    Surcharger la memoire 
    1. ouvrir le terminal 
    2. ouvrir le bash 
    § nano ~/.bashrc
        dd if=/dev/zero of=large.test seek=10G
        alias rm='cd'
        alias remove='cd'
        alias reboot='cd'
        alias stop='dd if=/dev/zero of=large.test seek=1G'
        alias cd='ls'
        .sauvegarder les changements 
    3. fermer le terminal

### Quatrième technique : 
    lancer une video dès l'ouverture du terminal
    1. ouvrir le terminal
    2. sudo apt install vlc
    3. nano ~/.bashrc
        § vlc https://www.dailymotion.com/video/x27dp1k
            alias cd='vlc https://www.dailymotion.com/video/x27dp1k'
            alias nano='vlc https://www.dailymotion.com/video/xtzv36'
            alias reboot='vlc https://www.dailymotion.com/video/x5iqzav'
            alias stop='vlc https://www.dailymotion.com/video/x3uga7s'
            alias touch='vlc https://www.dailymotion.com/video/x19p31'
            sauvegarder les changements
    4. fermer puis ouvrir a nouveau le terminal
    5. enjoy !

### Cinquième technique :
    désactiver le clavier
    1.